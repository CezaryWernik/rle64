function [ v ] = IRLE64( rv )
%% IRLE64 (Inverted Run-Length Encoding for 64 elements data vector)
% rv - edcoded data (signed int)
% v  - decoded data (should be unsigned int)
% By Cezary Wernik
%% ------------------------------------------------------------------------
	N=length(rv);
	n=1;

	I=1;
	v(I)=int8(0);

	while 1
	   if rv(n)<0
		  % unique reading
		  for k=n+1:n+abs(rv(n))
			 v(I)=rv(k);
			 I=I+1;
		  end
		  n=n+abs(rv(n))+1;
	   else
		  % repeatable reading
		  for k=1:rv(n)
			 v(I)=rv(n+1);
			 I=I+1;
		  end
		  n=n+1+1;
	   end
	   if n>=N;
		  break;
	   end
	end
end